const fs = require("fs");

module.exports = problem2;

function problem2(testFile) {
  fs.readFile(testFile, "utf-8", function (err, data) {
    if (err) {
      console.error(err);
    } else {
      data = data.toUpperCase();
      fs.writeFile("uppercase.txt", data, function (err) {
        if (err) {
          console.error(err);
          return;
        } else {
          console.log('Created "uppercase.txt"');
          fs.writeFile("filenames.txt", "uppercase.txt\n", function (err) {
            
            if (err) {
              console.error(err);
              return;
            } else {
              console.log('Created "filenames.txt"');
            }
          });

          fs.readFile("uppercase.txt", "utf-8", function (err, data) {
            if (err) {
              console.error(err);
              return;
            } else {
              data = data.toLowerCase().split(".");
              fs.writeFile(
                "lowercaseSplit.txt",
                JSON.stringify(data, null, 2),
                function (err) {
                  if (err) {
                    console.error(err);
                    return;
                  } else {
                    console.log('Created "lowercaseSplit.txt"');
                    fs.appendFile(
                      "filenames.txt",
                      "lowercaseSplit.txt\n",
                      function (err) {
                       
                        if (err) {
                          console.error(err);
                          return;
                        } else {
                          console.log('Updated "filenames.txt"');
                        }
                      }
                    );

                    fs.readFile("lowercaseSplit.txt", "utf-8", function(err, data){
                      if (err) {
                        console.error(err);
                      } else {
                        data = JSON.parse(data).sort();
                        fs.writeFile(
                          "lowercaseSplitSort.txt",
                          JSON.stringify(data, null, 2),
                          function(err){
                            if (err) {
                              console.error(err);
                            } else {
                              console.log('Created "lowercaseSplitSort.txt"');
                              fs.appendFile(
                                "filenames.txt",
                                "lowercaseSplitSort.txt\n",
                                function(err){
                                  
                                  if (err) {
                                    console.error(err);
                                    return;
                                  } else {
                                    console.log('Updated "filenames.txt"');
                                    fs.readFile(
                                      "filenames.txt",
                                      "utf-8",
                                      function(err, data){
                                        if (err) {
                                          console.error(err);
                                          return;
                                        } else {
                                          data = data
                                            .trim()
                                            .split(/\s/)
                                            .forEach((file) => {
                                              fs.unlink(file, function(err){
                                                if (err) {
                                                  console.error(err);
                                                  return;
                                                } else {
                                                  console.log(`Deleted "${file}"`);
                                                }
                                              });
                                            });
                                        }
                                      }
                                    );
                                  }
                                }
                              );
                            }
                          }
                        );
                      }
                    });
                  }
                }
              );
            }
          });
        }
      });
    }
  });
}

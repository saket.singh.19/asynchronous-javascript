
const cards = require ('./data/cards.json')

function callback3(listId, cb){
    setTimeout(() => {
        let data;
        for (let key in cards) {
            if (key == listId) {
                data = cards[key];
            }
        }

        if (data) {
            cb(data)
        } 
    }, 2000);
}

module.exports = callback3;
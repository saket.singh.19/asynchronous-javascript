
const lists = require ('./data/lists.json')


function callback2(boardID, cb){
    setTimeout(() => {
        let data;
        for (let key in lists) {
             if (key == boardID) {
                data = lists[key];
            }
        }

        if (data) {
            cb(data)
        
        }
    }, 2000);
}

module.exports = callback2;
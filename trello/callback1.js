
const boards = require('./data/boards.json');

function callback1 (boardID, cb){
   
    setTimeout(() => {
        let data;
        for (let i = 0; i < boards.length; i++) {
            if (boards[i].id == boardID) {
                data = boards[i];
                break;
            }
        }
        if (data) {
            cb(data)
        } 
        
    }, 2000);
      


}

module.exports = callback1;
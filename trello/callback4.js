
const callback2 = require('./callback2');
const callback3 = require('./callback3');
const boards = require('./data/boards.json');


function callback4(){
   
    let thanos = boards.find((boardValue) => boardValue.name === 'Thanos');
    let thanosId = thanos.id;

    callback2(thanosId, function(result){
        let mindId = result.find(data => data.name === 'Mind').id;
        callback3(mindId, function(result2){
            console.log(thanos,result,result2)
        });
    });
}

module.exports = callback4;